# Automated Reconciliation Project

## Name
Automated Reconciliation

## Description
- This project came about when I was offered a position in a financial department where I had to reconcile accounts that were multi-thousands of lines long on an almost weekly basis. The stipulations that made the job tedious were: the fact that the data could not be sorted in some cases, more often than not the orientation of the data was different between the files, and finally that complete accuracy and speed were required. Based on these criteria I created this reconciliation as a side project to test my skills I was gathering through college and bridge the gap between my financial education and my desire to enter the field of data and data analysis.
## Visuals

![Whiteboard 1](Whiteboard_3.jpg)
![Whiteboard 2](Whiteboard_2.jpg)
![Whiteboard 3](Whiteboard_4.jpg)
![Whiteboard 4](Whiteboard_1.jpg)


## Roadmap
- Add in indexing for final output to eliminate record of accounting strings that passed the tests
- Add in PDF reader for converting PDF to dataframe for reconciliation
- Add in some means of sorting by unique identifiers so that the data does not need to be sorted

## Authors and acknowledgment
- Mark Rojas: Architect
- Curtis Andersen: Consultant
- Daniel Floratos: Consultant


## Project status
Work in progress
